import { Component, OnInit  } from '@angular/core';
import {SMS} from "@ionic-native/sms";
import { NavController } from 'ionic-angular';
import { Observable } from 'rxjs/Observable';
import {MessageInterface} from "./homeinterface";
import {HomeService} from "./homeservice";
import {Validators, FormBuilder, FormGroup } from '@angular/forms';
import { GooglePlus } from '@ionic-native/google-plus';
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  providers: [GooglePlus]
})
export class HomePage implements OnInit{
  phone :Observable<any>;
  message :Observable<any>;
  products: MessageInterface[];
  errorMessage:string;
  phonenum:string;
  product = new MessageInterface();
  private todo : FormGroup;
  displayName: any;
  email: any;
  familyName: any;
  givenName: any;
  userId: any;
  imageUrl: any;

  isLoggedIn:boolean = false;

  constructor(private formBuilder: FormBuilder,private sms: SMS,public navCtrl: NavController,private homeService: HomeService, private googlePlus: GooglePlus) {
    this.todo = this.formBuilder.group({
      phonenum: ['', Validators.compose([Validators.required,Validators.pattern('[0-9]*'), Validators.maxLength(10), Validators.minLength(10)])],
      textmsg: ['',  Validators.compose([Validators.required])],
    });

  }

  login(){
    this.googlePlus.login({
/*      'scopes':'https://www.googleapis.com/auth/gmail.readonly',*/
      'webClientId': '584228619291-q4i02u9pk5u8bomrjg200ku8gv49ff6n.apps.googleusercontent.com',
      'offline': true
    })
      .then(res => {
        console.log(res);
        this.displayName = res.displayName;
        this.email = res.email;
        this.familyName = res.familyName;
        this.givenName = res.givenName;
        this.userId = res.userId;
        this.imageUrl = res.imageUrl;

        this.isLoggedIn = true;
      })
      .catch(err => console.error(err));
  }

  logout() {
    this.googlePlus.logout()
      .then(res => {
        console.log(res);
        this.displayName = "";
        this.email = "";
        this.familyName = "";
        this.givenName = "";
        this.userId = "";
        this.imageUrl = "";

        this.isLoggedIn = false;
      })
      .catch(err => console.error(err));
  }


  emaildata():void{

  }


  validate():boolean{
    if (this.todo.valid) {
      return true;
    }

    var phonenum = this.todo.controls['phonenum'];
    var textmsg = this.todo.controls['textmsg'];
    if (!phonenum.valid) {
      if (phonenum.errors['required']) {
        alert('Provide a phonenumber please');
      } else if (phonenum.errors['maxlength']) {
        alert('The phone number cannot be more than 10 digits');
      } else if (phonenum.errors['pattern']){
        alert('Please input only numbers');
      } else if (phonenum.errors['minlength']){
        alert('The phone number cannot be less then 10 digits');
      }
    }
    if (!textmsg.valid) {
      if (textmsg.errors['required']) {
        alert('Provide a message please');
      }
    }
    return false;
  }


  sendTextMessage():void{
    if (this.validate()) {
      this.homeService.putProducts(this.product).subscribe(product => {
       this.fetchmessage();
        this.reset();
        this.phonenum = product.number;
        console.log('Message added to database successfully!');
        console.log(this.product);
      }, error => {
        return this.errorMessage = <any>error;
      });
    }
  }

  private reset() {
    this.product.number = null;
    this.product.message = null;
    this.errorMessage = null;
    this.phonenum = null;
  }




  sendTextMessage1(){
    var options: {
      replaceLineBreaks: true,
      android: {
        intent: "INTENT",
      }
    };
    if (this.validate()) {
      if (this.products.length < 100) {
        console.log(this.products.length);
        this.sms.send(String(this.product.number), this.product.message, options).then(() => (
          alert("Message Sent!")
        )).catch((err) => (
          alert("Message Not Sent!: " + err)
        ));
      }
      else {
        alert("Message Not Sent!: 100 Messages Limit Reached");
      }
    }
  }


  ngOnInit(): void {
  this.fetchmessage();
  this.emaildata();
  }

 fetchmessage(): void {
    this.homeService.getProducts()
      .subscribe(products => {
        this.products = products;
        console.log(this.products.length);
      } , error => {
        return this.errorMessage = <any>error;
      });

  }

}
