import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { MessageInterface } from './homeinterface';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';
import {HttpJsonParseError} from '@angular/common/http/src/response';

@Injectable()
export class HomeService {



  constructor(private _http: HttpClient) {}




  getProducts(): Observable<MessageInterface[]> {
    return this._http.get<MessageInterface[]>('http://192.168.43.150:8181/service/numbers/get',{ headers: { 'Content-Type': 'application/json' } }).do(data => {
      console.log('ALL: ' + JSON.stringify(data));

    })
      .catch(this.handleError1);
  }



  putProducts(product: MessageInterface): Observable<MessageInterface> {
    return this._http.get('http://192.168.43.150:8181/service/numbers/get?product='+product.number+product.message,{ headers: { 'Content-Type': 'application/json' } }).do(data => {
      console.log('ALL: ' + JSON.stringify(data));

    })
      .catch(this.handleError2);
  }

  private handleError2(err: HttpJsonParseError)
  {
    console.log('putproducts error',err);
    return Observable.throw(err);
  }

  private handleError1(err: HttpJsonParseError)
  {
    console.log('getproducts error',err);
    return Observable.throw(err);
  }



}
